This is GotPrint test
Here are the details of Tasks

Task1
Create a profiles listing page that dynamically populates user data from the following JSON endpoint
Steps of creating functionality
1) I have created users.html file
2) Used Gmap Api for showing Google Map in users list
3) Created loadJSON function which sends request to api end point and fetch data from Api
4) Once Data retrieved I filtered data according to even id's
5) Used Javascript sort function to sort data according to user's last name.
Files Used:
1) users.html
2) style.css

Task2
Adding Shapes to Canvas using Fabric js
Steps of creating functionality
1) Created fabric.html file
2) Created Select box to show Canvas 1 to 5
3) On Change of Canvas select box, using javascript I have created elements Shape and Add Button
4) On Selecting Canvas and Shape and clicking on Button, I have triggered canvas shapes.

Files Used:
1) fabric.html
2) fabric.css
3) fabric.js
